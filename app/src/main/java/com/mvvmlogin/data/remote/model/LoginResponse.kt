package com.mvvmlogin.data.remote.model

import com.google.gson.annotations.SerializedName

data class LoginResponse(
    @SerializedName("company_phone")
    val companyPhone: String? = null,
    @SerializedName("can_user_buy")
    val canUserBuy: String? = null,
    @SerializedName("user_email")
    val userEmail: String? = null,
    @SerializedName("user_is_buyer")
    val isUserBuyer: String? = null,
    @SerializedName("user_is_vendor")
    val isUserVendor: String? = null,
    @SerializedName("user_image")
    val userImage: String? = null,
    @SerializedName("can_user_memo")
    val canUserMemo: String? = null,
    @SerializedName("user_name")
    val userName: String? = null,
    @SerializedName("company_name")
    val companyName: String? = null,
    @SerializedName("company_email")
    val companyEmail: String? = null,
    @SerializedName("can_user_bid")
    val canUserBid: String? = null,
    @SerializedName("user_token")
    val userToken: String? = null

)
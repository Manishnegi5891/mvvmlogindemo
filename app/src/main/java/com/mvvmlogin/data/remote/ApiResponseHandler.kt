package com.mvvmlogin.data.remote

import retrofit2.Response

interface ApiResponseHandler {
    fun onApiSuccess(tag: Int, response: Response<*>?)
    fun onApiFailure(tag: Int, t: Throwable?)
}
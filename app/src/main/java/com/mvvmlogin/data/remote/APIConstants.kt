package com.mvvmlogin.data.remote

object APIConstants {
    private const val DOMAIN_NAME = "http://apitestdev.dfl.4livedemo.com/" //feature test
    private const val API_VERSION = "api/1.02/"
    const val BASE_URL = "$DOMAIN_NAME$API_VERSION"

    //API Tags
    const val LOGIN_API = 1
}
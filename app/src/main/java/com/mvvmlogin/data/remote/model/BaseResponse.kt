package com.mvvmlogin.data.remote.model

import com.google.gson.annotations.SerializedName

data class BaseResponse<T>(
    @SerializedName("msg")
    val msg: String? = null,
    @SerializedName("status")
    val status: String? = null
) {

    @SerializedName("data")
    val data: T? = null

}
package com.mvvmlogin.data.remote

import com.mvvmlogin.data.remote.model.BaseResponse
import com.mvvmlogin.data.remote.model.LoginResponse
import retrofit2.Call
import retrofit2.http.*
import java.util.*

interface APIEndPoints {


    @FormUrlEncoded
    @Headers("devicetype: 1") //android
    @POST("guest-user/login")
    fun logIn(@Header("deviceid") deviceID: String?, @FieldMap logInData: HashMap<String, String?>): Call<BaseResponse<LoginResponse>>


}
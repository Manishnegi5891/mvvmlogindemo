package com.mvvmlogin.ui.login

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.mvvmlogin.data.remote.ApiResponseHandler
import com.mvvmlogin.data.remote.APIConstants.LOGIN_API
import com.mvvmlogin.data.remote.model.BaseResponse
import com.mvvmlogin.data.remote.model.LoginResponse
import com.mvvmlogin.ui.base.view_model.BaseViewModel
import retrofit2.Response


class LoginViewModel(application: Application) : BaseViewModel(application),
    ApiResponseHandler {
    private val repository = LoginRepository()
    var email: MutableLiveData<String> = MutableLiveData("appuser@dummyid.com")
    var password: MutableLiveData<String> = MutableLiveData("appuser@123")
    var loginResponse: MutableLiveData<BaseResponse<LoginResponse>> = MutableLiveData()
    fun performLogin() {
        repository.loginUser(email.value, password.value, this, LOGIN_API)
    }


    override fun onApiSuccess(tag: Int, response: Response<*>?) {
        when (tag) {
            LOGIN_API -> {
                loginResponse.value = response?.body() as BaseResponse<LoginResponse>

            }
        }

    }

    override fun onApiFailure(tag: Int, t: Throwable?) {
        println("LoginViewModel.onApiFailure")

    }


}

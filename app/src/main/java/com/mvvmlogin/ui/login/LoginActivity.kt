package com.mvvmlogin.ui.login

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.mvvmlogin.R
import com.mvvmlogin.databinding.ActivityMainBinding

class LoginActivity : AppCompatActivity() {
    private lateinit var vmLogin: LoginViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding =
            DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main)
        vmLogin = ViewModelProviders.of(this).get(LoginViewModel::class.java)
        binding.vmLogin = vmLogin

        vmLogin.loginResponse.observe(
            this,
            Observer { Toast.makeText(this, it.msg, Toast.LENGTH_LONG).show() })
    }

}

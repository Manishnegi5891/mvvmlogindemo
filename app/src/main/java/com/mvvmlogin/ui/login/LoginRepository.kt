package com.mvvmlogin.ui.login

import com.mvvmlogin.data.remote.ApiResponseHandler
import com.mvvmlogin.data.remote.RetrofitInstance
import com.mvvmlogin.data.remote.model.BaseResponse
import com.mvvmlogin.data.remote.model.LoginResponse
import com.mvvmlogin.ui.base.BaseRepository
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginRepository : BaseRepository() {

    fun loginUser(email: String?, pwd: String?, callback: ApiResponseHandler, tag: Int) {
        val deviceId = "23232323"
        val param = HashMap<String, String?>()
        param["email"] = email
        param["password"] = pwd
        RetrofitInstance.retrofitInstance?.logIn(deviceId, param)
            ?.enqueue(object : Callback<BaseResponse<LoginResponse>> {
                override fun onFailure(call: Call<BaseResponse<LoginResponse>>, t: Throwable) {
                    println("LoginRepository.onFailure")
                    println(t.message)
                    callback.onApiFailure(tag, t)
                }

                override fun onResponse(
                    call: Call<BaseResponse<LoginResponse>>,
                    response: Response<BaseResponse<LoginResponse>>
                ) {
                    callback.onApiSuccess(tag, response = response)
                }
            })


    }


}